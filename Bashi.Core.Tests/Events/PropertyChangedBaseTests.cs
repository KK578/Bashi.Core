using System;
using Bashi.Core.Events;
using NUnit.Framework;

namespace Bashi.Core.Tests.Events
{
    [TestFixture]
    public class PropertyChangedBaseTests
    {
        private class StubPropertyChanged : PropertyChangedBase
        {
            private string basicString;

            public string BasicString
            {
                get => basicString;
                set => SetAndRaiseOnPropertyChanged(() => BasicString, ref basicString, value);
            }

            public void RaiseBasicStringPropertyChanged()
            {
                OnPropertyChanged(() => BasicString);
            }
        }

        [Test]
        public void Dispose_ShouldDisposeTheUnderlyingSubject()
        {
            var subject = new StubPropertyChanged();
            subject.Dispose();

            Assert.That(() => subject.PropertyChanged.Subscribe(x => Assert.Fail()),
                        Throws.Exception);
        }

        [Test]
        public void OnPropertyChanged_GivenForcedRaiseOfPropertyChanged_ShouldCallSubscription()
        {
            var subject = new StubPropertyChanged();
            string propertyName = null;
            subject.PropertyChanged.Subscribe(x => propertyName = x.PropertyName);
            subject.RaiseBasicStringPropertyChanged();

            Assert.That(propertyName, Is.EqualTo("BasicString"));
        }

        [Test]
        public void SetAndRaiseOnPropertyChanged_GivenPropertyIsSetToNewValue_ShouldSetValueAndCallSubscription()
        {
            var subject = new StubPropertyChanged();
            subject.BasicString = "Hello";
            string propertyName = null;
            subject.PropertyChanged.Subscribe(x => propertyName = x.PropertyName);
            subject.BasicString = "World";

            Assert.That(subject.BasicString, Is.EqualTo("World"));
            Assert.That(propertyName, Is.EqualTo("BasicString"));
        }

        [Test]
        public void SetAndRaiseOnPropertyChanged_GivenPropertyIsSetToCurrentValue_ShouldNotCallSubscription()
        {
            var subject = new StubPropertyChanged();
            subject.BasicString = "Hello";
            string propertyName = null;
            subject.PropertyChanged.Subscribe(x => propertyName = x.PropertyName);
            subject.BasicString = "Hello";

            Assert.That(subject.BasicString, Is.EqualTo("Hello"));
            Assert.That(propertyName, Is.Null);
        }
    }
}
