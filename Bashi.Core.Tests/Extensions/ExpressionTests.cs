using System;
using System.Linq.Expressions;
using Bashi.Core.Extensions;
using NUnit.Framework;

namespace Bashi.Core.Tests.Extensions
{
    [TestFixture]
    public class ExpressionTests
    {
        [Test]
        public void GetPropertyName_GivenProperty_ShouldReturnNameOfProperty()
        {
            // ReSharper disable once ConvertToConstant.Local
            var testProperty = 1;
            Expression<Func<int>> property = () => testProperty;
            Assert.That(property.GetPropertyName(), Is.EqualTo(nameof(testProperty)));
        }
    }
}