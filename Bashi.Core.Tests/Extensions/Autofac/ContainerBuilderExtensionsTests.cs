using Autofac;
using Bashi.Core.Extensions.Autofac;
using NUnit.Framework;

namespace Bashi.Core.Tests.Extensions.Autofac
{
    [TestFixture]
    public class ContainerBuilderExtensionsTests
    {
        private class TestModule : Module
        {
            public bool ShouldRegister { get; set; }

            protected override void Load(ContainerBuilder builder)
            {
                base.Load(builder);

                if (ShouldRegister)
                {
                    builder.RegisterInstance("Hello");
                }
            }
        }

        [Test]
        public void RegisterModule_WithAction_ShouldManipulateModuleRegistration()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<TestModule>(x => x.ShouldRegister = true);

            var container = builder.Build();
            var result = container.Resolve<string>();

            Assert.That(result, Is.EqualTo("Hello"));
        }
    }
}
