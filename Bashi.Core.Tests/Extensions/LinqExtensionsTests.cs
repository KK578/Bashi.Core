using System.Collections.Generic;
using Bashi.Core.Extensions;
using NUnit.Framework;

namespace Bashi.Core.Tests.Extensions
{
    [TestFixture]
    public class LinqExtensionsTests
    {
        private IEnumerable<int> nullValues;
        private IEnumerable<int> values;

        [SetUp]
        public void SetUp()
        {
            nullValues = null;
            values = new List<int> { 1, 2, 3 };
        }

        [Test]
        public void ForEach_GivenNull_ShouldNotApplyTheAction()
        {
            var count = 0;
            nullValues.ForEach(x => count++);

            Assert.That(count, Is.Zero);
        }

        [Test]
        public void ForEach_GivenListOfValues_ShouldApplyActionToEachValue()
        {
            var count = 0;
            values.ForEach(x => count++);

            Assert.That(count, Is.EqualTo(3));
        }

        [Test]
        public void NullToEmpty_GivenNull_ShouldReturnANewEmptyEnumerable()
        {
            var enumerable = nullValues.NullToEmpty();
            Assert.That(enumerable, Is.Not.Null);
            Assert.That(enumerable, Is.Empty);
        }

        [Test]
        public void NullToEmpty_GivenListOfValues_ShouldReturnGivenList()
        {
            var enumerable = values.NullToEmpty();
            Assert.That(enumerable, Is.Not.Null);
            Assert.That(enumerable, Is.SameAs(values));
        }

        [Test]
        public void Batch_GivenNull_ShouldReturnNull()
        {
            Assert.That(nullValues.Batch(1), Is.Empty);
        }

        [Test]
        public void Batch_GivenZeroBatchSize_ShouldReturnNull()
        {
            Assert.That(values.Batch(0), Is.Empty);
        }

        [Test]
        public void Batch_GivenListOfValues_AndBatchSizeOne_ShouldSplitListIntoThreeLists()
        {
            Assert.That(values.Batch(1),
                        Is.EquivalentTo(new[]
                                        {
                                            new[] { 1 },
                                            new[] { 2 },
                                            new[] { 3 },
                                        }));
        }

        [Test]
        public void Batch_GivenListOfValues_AndBatchSizeTwo_ShouldSplitListIntoTwoLists()
        {
            Assert.That(values.Batch(2),
                        Is.EquivalentTo(new[]
                                        {
                                            new[] { 1, 2 },
                                            new[] { 3 },
                                        }));
        }
    }
}