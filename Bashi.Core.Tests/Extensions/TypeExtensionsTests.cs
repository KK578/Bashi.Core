using Bashi.Core.Extensions;
using NUnit.Framework;

namespace Bashi.Core.Tests.Extensions
{
    [TestFixture]
    public class TypeExtensionsTests
    {
        private class BaseType
        {
        }

        private class DerivedType : BaseType
        {
        }

        [Test]
        public void IsAssignableTo_GivenBaseType_WhenTypeDerivesFromBaseType_ShouldReturnTrue()
        {
            var result = typeof(DerivedType).IsAssignableTo<BaseType>();
            Assert.That(result, Is.True);
        }

        [Test]
        public void IsAssignableTo_GivenDerivedType_WhenCheckingBaseType_ShouldReturnFalse()
        {
            var result = typeof(BaseType).IsAssignableTo<DerivedType>();
            Assert.That(result, Is.False);
        }
    }
}
