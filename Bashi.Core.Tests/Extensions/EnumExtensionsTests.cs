using Bashi.Core.Extensions;
using NUnit.Framework;

namespace Bashi.Core.Tests.Extensions
{
    [TestFixture]
    public class EnumExtensionsTests
    {
        private enum TestEnum
        {
            [System.ComponentModel.Description("First Tester")]
            Test1,
            Test2,
        }

        [Test]
        public void GetDescription_GivenValueWithDescriptionAttribute_ShouldGiveDescription()
        {
            Assert.That(TestEnum.Test1.GetDescription(), Is.EqualTo("First Tester"));
        }

        [Test]
        public void GetDescription_GivenValueWithNoDescriptionAttribute_ShouldGiveEnumName()
        {
            Assert.That(TestEnum.Test2.GetDescription(), Is.EqualTo("Test2"));
        }
    }
}