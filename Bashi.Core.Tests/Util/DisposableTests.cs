using Bashi.Core.Util;
using NUnit.Framework;

namespace Bashi.Core.Tests.Util
{
    [TestFixture]
    public class DisposableTests
    {
        private static int disposeCount;

        private class TestDisposable : Disposable
        {
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                disposeCount++;
            }

            public bool Disposed => base.IsDisposed;
        }

        [SetUp]
        public void SetUp()
        {
            disposeCount = 0;
        }

        [Test]
        public void Dispose_ShouldCallDisposeMethod()
        {
            var subject = new TestDisposable();
            subject.Dispose();

            Assert.That(disposeCount, Is.EqualTo(1));
            Assert.That(subject.Disposed, Is.True);
        }

        [Test]
        public void Dispose_WhenCalledMultipleTimes_WillOnlyDisposeOnce()
        {
            var subject = new TestDisposable();
            subject.Dispose();
            subject.Dispose();

            Assert.That(disposeCount, Is.EqualTo(1));
        }
    }
}