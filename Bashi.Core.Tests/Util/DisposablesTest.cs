using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Subjects;
using Bashi.Core.Events;
using Bashi.Core.Util;
using Moq;
using NUnit.Framework;

namespace Bashi.Core.Tests.Util
{
    [TestFixture]
    public class DisposablesTest
    {
        private class TestDisposables : Disposables
        {
            public string LastPropertyName { get; private set; }

            public TestDisposables(IObservable<int> observable, Func<IObservable<int>, IDisposable> func)
            {
                Subscribe(observable, func);
            }

            public TestDisposables(IObservablePropertyChanged propertyChanged)
            {
                SubscribeToPropertyChanged(propertyChanged, () => LastPropertyName, () => LastPropertyName = "Banana");
            }
        }

        [Test]
        public void Subscribe_GivenSubject_ShouldRunSubscription()
        {
            var observer = new Subject<int>();
            var numbers = new List<int>();
            var subject = new TestDisposables(observer, x => x.Subscribe(i => numbers.Add(i)));
            observer.OnNext(1);
            observer.OnNext(2);
            observer.OnNext(3);

            Assert.That(numbers, Is.EquivalentTo(new[] { 1, 2, 3 }));
            subject.Dispose();
        }

        [Test]
        public void Subscribe_AfterDispose_ShouldNoLongerRunSubscription()
        {
            var observer = new Subject<int>();
            var numbers = new List<int>();
            var subject = new TestDisposables(observer, x => x.Subscribe(i => numbers.Add(i)));
            observer.OnNext(1);
            subject.Dispose();
            observer.OnNext(2);
            observer.OnNext(3);

            Assert.That(numbers, Is.EquivalentTo(new[] { 1 }));
        }

        [Test]
        public void SubscribeToPropertyChanged_WhenRaisingPropertyChangeEvent_ShouldRunSubscription()
        {
            var observable = new Subject<PropertyChangedEventArgs>();
            var propertyChanged = Mock.Of<IObservablePropertyChanged>(x => x.PropertyChanged == observable);

            var subject = new TestDisposables(propertyChanged);
            observable.OnNext(new PropertyChangedEventArgs("LastPropertyName"));

            Assert.That(subject.LastPropertyName, Is.EqualTo("Banana"));
        }
    }
}