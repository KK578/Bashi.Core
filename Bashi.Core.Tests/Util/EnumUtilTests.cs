using Bashi.Core.Util;
using NUnit.Framework;

namespace Bashi.Core.Tests.Util
{
    [TestFixture]
    public class EnumUtilTests
    {
        private enum TestEnum
        {
            [System.ComponentModel.Description("First Tester")]
            Test1,
            Test2,
            Test3
        }

        [Test]
        public void Parse_ShouldGiveCorrectValue()
        {
            var result = EnumUtil.Parse<TestEnum>("Test1", false);
            Assert.That(result, Is.EqualTo(TestEnum.Test1));
        }

        [Test]
        public void Parse_WithIgnoreCase_ShouldGiveCorrectValue()
        {
            var result = EnumUtil.Parse<TestEnum>("test1");
            Assert.That(result, Is.EqualTo(TestEnum.Test1));
        }

        [Test]
        public void ParseWithDescription_ShouldGiveCorrectValue()
        {
            var result = EnumUtil.ParseWithDescription<TestEnum>("First Tester", false);
            Assert.That(result, Is.EqualTo(TestEnum.Test1));
        }

        [Test]
        public void ParseWithDescription_WithIgnoreCase_ShouldGiveCorrectValue()
        {
            var result = EnumUtil.ParseWithDescription<TestEnum>("first tester");
            Assert.That(result, Is.EqualTo(TestEnum.Test1));
        }

        [Test]
        public void ParseWithDescription_WhenDescriptionDoesNotMatch_ShouldFallbackToParse()
        {
            var result = EnumUtil.ParseWithDescription<TestEnum>("Test1");
            Assert.That(result, Is.EqualTo(TestEnum.Test1));
        }

        [Test]
        public void GetValues_ShouldGiveListOfEnums()
        {
            var result = EnumUtil.GetValues<TestEnum>();
            Assert.That(result, Is.EquivalentTo(new[] { TestEnum.Test1, TestEnum.Test2, TestEnum.Test3 }));
        }
    }
}