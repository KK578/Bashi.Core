using Bashi.Core.Logging;
using NUnit.Framework;

namespace Bashi.Core.Tests.Logging
{
    [TestFixture]
    public class LogManagerTests
    {
        [Test]
        public void CreateForCurrentClass_WhenCalled_ShouldCreateLoggerForTestClass()
        {
            var log = LogManager.CreateForCurrentClass();
            Assert.That(log.Name, Is.EqualTo(nameof(LogManagerTests)));
        }
    }
}
