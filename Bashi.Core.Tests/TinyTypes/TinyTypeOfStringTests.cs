using Bashi.Core.TinyTypes;
using NUnit.Framework;

namespace Bashi.Core.Tests.TinyTypes
{
    [TestFixture]
    public class TinyTypeOfStringTests
    {
        [Test]
        public void Equality_GivenSameValue_AsTinyTypeAndAsRealType_ShouldAlwaysBeTrue()
        {
            const string s = "Banana";
            var tt = new TinyTypeOfString(s);
            var tt1 = new TinyTypeOfString(s);
            Assert.That(s.Equals(tt), Is.True);
            Assert.That(s == tt, Is.True);
            Assert.That(tt == tt1, Is.True);
            Assert.That(tt.Equals((object)tt), Is.True);
        }

        [Test]
        public void ObjectOverrides_ShouldFallThroughToRealObject()
        {
            const string s = "Banana";
            var tt = new TinyTypeOfString(s);

            Assert.That(s.ToString(), Is.EqualTo(tt.ToString()));
            Assert.That(s.GetHashCode(), Is.EqualTo(tt.GetHashCode()));
        }
    }
}