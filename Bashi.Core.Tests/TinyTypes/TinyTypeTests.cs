using Bashi.Core.TinyTypes;
using NUnit.Framework;

namespace Bashi.Core.Tests.TinyTypes
{
    [TestFixture]
    public class TinyTypeTests
    {
        private class Author
        {
            private readonly string firstName;
            private readonly string lastName;

            public Author(string firstName, string lastName)
            {
                this.firstName = firstName;
                this.lastName = lastName;
            }

            public Author(FirstName firstName, LastName lastName)
                : this(firstName.Value, lastName.Value)
            {
            }

            public override string ToString()
            {
                return $"{firstName} {lastName}";
            }
        }

        private class FirstName : TinyType<string>
        {
            public FirstName(string value) : base(value)
            {
            }
        }

        private class LastName : TinyType<string>
        {
            public LastName(string value) : base(value)
            {
            }
        }

        [Test]
        public void ImplicitOperator_ShouldAutomaticallyConvertValues()
        {
            var tt = new TinyType<int>(7);
            int value = tt;
            Assert.That(value, Is.EqualTo(7));
        }

        [Test]
        public void Equals_GivenItself_ShouldAlwaysBeTrue()
        {
            var tt = new TinyType<int>(7);
            Assert.That(tt.Equals(7), Is.True);
            Assert.That(tt.Equals(tt), Is.True);
            Assert.That(tt.Equals((object)7), Is.True);
            Assert.That(tt.Equals((object)tt), Is.True);

            const string s = "Banana";
            var tt1 = new TinyType<string>(s);
            Assert.That(tt1.Equals(s), Is.True);
            Assert.That(tt1.Equals((object)s), Is.True);
        }

        [Test]
        public void Equals_GivenDifferentValue_ShouldAlwaysBeFalse()
        {
            var tt = new TinyType<int>(7);
            var tt1 = new TinyType<int>(10);
            Assert.That(tt.Equals(null), Is.False);
            Assert.That(tt.Equals(9), Is.False);
            Assert.That(tt.Equals(tt1), Is.False);
            Assert.That(tt.Equals("7"), Is.False);
        }

        [Test]
        public void Equals_GivenDefaultValues_ShouldBeTrue()
        {
            var tt = new TinyType<int>(default(int));
            Assert.That(tt.Equals(default(int)), Is.True);
        }

        [Test]
        public void GivenAuthorObjectUsingTinyTypes_ShouldProduceSameUnderlyingName()
        {
            var a1 = new Author("John", "Smith");
            var a2 = new Author(new FirstName("John"), new LastName("Smith"));

            Assert.That(a1.ToString(), Is.EqualTo(a2.ToString()));
        }

        [Test]
        public void ObjectOverrides_ShouldFallThroughToRealObject()
        {
            var array = new[] { 1, 2, 3 };
            var tt = new TinyType<int[]>(array);

            Assert.That(array.ToString(), Is.EqualTo(tt.ToString()));
            Assert.That(array.GetHashCode(), Is.EqualTo(tt.GetHashCode()));
        }
    }
}