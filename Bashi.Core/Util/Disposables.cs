using System;
using System.Linq.Expressions;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Bashi.Core.Events;
using Bashi.Core.Extensions;

namespace Bashi.Core.Util
{
    public abstract class Disposables : Disposable
    {
        private readonly CompositeDisposable disposable = new CompositeDisposable();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                disposable.Dispose();
            }
        }

        protected void Subscribe<T>(IObservable<T> observable, Func<IObservable<T>, IDisposable> subscription)
        {
            disposable.Add(subscription(observable));
        }

        protected void SubscribeToPropertyChanged<T>(IObservablePropertyChanged subject, Expression<Func<T>> expression, Action subscription)
        {
            var propertyName = expression.GetPropertyName();
            Subscribe(subject.PropertyChanged, x => x.Where(y => y.PropertyName == propertyName)
                                                     .Subscribe(_ => subscription()));
        }
    }
}