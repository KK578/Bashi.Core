using System;
using System.Collections.Generic;
using System.Linq;
using Bashi.Core.Extensions;

namespace Bashi.Core.Util
{
    public static class EnumUtil
    {
        public static TEnum Parse<TEnum>(string value, bool ignoreCase = true) where TEnum : Enum
        {
            return (TEnum)Enum.Parse(typeof(TEnum), value, ignoreCase);
        }

        public static TEnum ParseWithDescription<TEnum>(string input, bool ignoreCase = true) where TEnum : Enum
        {
            var values = GetValues<TEnum>();
            var comparisonMode = ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal;

            foreach (var value in values)
            {
                if (string.Equals(input, value.GetDescription(), comparisonMode))
                {
                    return value;
                }
            }

            return Parse<TEnum>(input, ignoreCase);
        }

        public static IEnumerable<TEnum> GetValues<TEnum>() where TEnum : Enum
        {
            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
        }
    }
}