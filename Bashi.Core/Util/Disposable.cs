using System;
using System.Threading;

namespace Bashi.Core.Util
{
    public abstract class Disposable : IDisposable
    {
        private const int DisposedFlag = 1;
        private int isDisposed;

        public void Dispose()
        {
            var wasDisposed = Interlocked.Exchange(ref isDisposed, DisposedFlag);
            if (wasDisposed == DisposedFlag)
            {
                return;
            }

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) { }

        protected bool IsDisposed
        {
            get
            {
                Interlocked.MemoryBarrier();
                return isDisposed == DisposedFlag;
            }
        }
    }
}