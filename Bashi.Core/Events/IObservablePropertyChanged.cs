using System;
using System.ComponentModel;

namespace Bashi.Core.Events
{
    public interface IObservablePropertyChanged
    {
        IObservable<PropertyChangedEventArgs> PropertyChanged { get; }
    }
}