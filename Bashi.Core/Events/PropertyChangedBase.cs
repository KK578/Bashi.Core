using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reactive.Subjects;
using Bashi.Core.Extensions;
using Bashi.Core.Util;

namespace Bashi.Core.Events
{
    public abstract class PropertyChangedBase : Disposables, IObservablePropertyChanged
    {
        private readonly Subject<PropertyChangedEventArgs> propertyChangedSubject;

        protected PropertyChangedBase()
        {
            propertyChangedSubject = new Subject<PropertyChangedEventArgs>();
        }

        public IObservable<PropertyChangedEventArgs> PropertyChanged => propertyChangedSubject;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                propertyChangedSubject.Dispose();
            }
        }

        protected void SetAndRaiseOnPropertyChanged<TProperty, T>(Expression<Func<TProperty>> expression, ref T value, T newValue)
        {
            if (EqualityComparer<T>.Default.Equals(value, newValue))
            {
                return;
            }

            value = newValue;
            OnPropertyChanged(expression);
        }

        protected void OnPropertyChanged<T>(Expression<Func<T>> expression)
        {
            var e = new PropertyChangedEventArgs(expression.GetPropertyName());
            propertyChangedSubject.OnNext(e);
        }
    }
}