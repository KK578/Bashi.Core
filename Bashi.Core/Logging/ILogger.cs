namespace Bashi.Core.Logging
{
    public interface ILogger
    {
        string Name { get; }
    }
}
