namespace Bashi.Core.Logging
{
    internal sealed class Logger : ILogger
    {
        public string Name { get; }

        public Logger(string name)
        {
            Name = name;
        }
    }
}