using System;
using System.Diagnostics;

namespace Bashi.Core.Logging
{
    public static class LogManager
    {
        public static ILogger CreateForCurrentClass()
        {
            var frame = new StackFrame(1);
            var method = frame.GetMethod();

            if (method == null)
            {
                throw new InvalidOperationException("There was no calling method to create Logger.");
            }

            var type = method.DeclaringType;
            if (type == null)
            {
                throw new InvalidOperationException("There was no declared type to create Logger.");
            }

            var className = type.Name;
            return new Logger(className);
        }
    }
}
