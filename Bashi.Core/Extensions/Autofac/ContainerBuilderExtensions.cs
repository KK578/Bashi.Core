using System;
using Autofac;
using Autofac.Core.Registration;

namespace Bashi.Core.Extensions.Autofac
{
    public static class ContainerBuilderExtensions
    {
        public static IModuleRegistrar RegisterModule<T>(this ContainerBuilder builder, Action<T> action) where T : Module, new()
        {
            var module = new T();
            action(module);

            return builder.RegisterModule(module);
        }
    }
}
