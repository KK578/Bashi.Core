using System;
using System.ComponentModel;
using System.Reflection;

namespace Bashi.Core.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription<TEnum>(this TEnum value) where TEnum : Enum
        {
            var fieldInfo = typeof(TEnum).GetField(value.ToString());
            var attribute = fieldInfo.GetCustomAttribute<DescriptionAttribute>();
            var result = attribute?.Description ?? value.ToString();

            return result;
        }
    }
}