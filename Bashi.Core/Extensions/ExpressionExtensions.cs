using System;
using System.Linq.Expressions;

namespace Bashi.Core.Extensions
{
    public static class ExpressionExtensions
    {
        public static string GetPropertyName<TProperty>(this Expression<Func<TProperty>> property)
        {
            return property.Body is MemberExpression memberExpression
                       ? memberExpression.Member.Name
                       : "";
        }
    }
}