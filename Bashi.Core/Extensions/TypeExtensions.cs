using System;

namespace Bashi.Core.Extensions
{
    public static class TypeExtensions
    {
        public static bool IsAssignableTo<T>(this Type t)
        {
            return typeof(T).IsAssignableFrom(t);
        }
    }
}
