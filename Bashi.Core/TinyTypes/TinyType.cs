using System;

namespace Bashi.Core.TinyTypes
{
    public class TinyType<T> : IEquatable<T>, IEquatable<TinyType<T>>
    {
        public TinyType(T value)
        {
            Value = value;
        }

        public T Value { get; }

        public static implicit operator T(TinyType<T> arg) => arg != null ? arg.Value : default;

        public bool Equals(T other)
        {
            var def = default(T);
            if (other.Equals(def) && Value.Equals(def))
            {
                return true;
            }

            return other.Equals(Value);
        }

        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case T val:
                    return AreEqual(val, Value);
                case TinyType<T> tt:
                    return Equals(tt);
                default:
                    return false;
            }
        }

        private static bool AreEqual(T t1, T t2)
        {
            if (ReferenceEquals(t1, t2))
            {
                return true;
            }

            return t1.Equals(t2);
        }

        public override int GetHashCode() => Value.GetHashCode();

        public override string ToString() => Value.ToString();

        public bool Equals(TinyType<T> other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return AreEqual(Value, other.Value);
        }
    }
}
