// ReSharper disable RedundantOverriddenMember

namespace Bashi.Core.TinyTypes
{
    public class TinyTypeOfString : TinyType<string>
    {
        public TinyTypeOfString(string t) : base(t)
        {
        }

        public static implicit operator string(TinyTypeOfString arg) => arg != null ? arg.Value : null;

        public override bool Equals(object obj) => base.Equals(obj);

        public override int GetHashCode() => base.GetHashCode();

        public override string ToString() => base.ToString();

        public static bool operator ==(TinyTypeOfString lhs, TinyTypeOfString rhs)
        {
            if (lhs is null || rhs is null)
            {
                return lhs is null && rhs is null;
            }

            return lhs.Value.Equals(rhs.Value);
        }

        public static bool operator !=(TinyTypeOfString lhs, TinyTypeOfString rhs) => !(lhs == rhs);
    }
}
