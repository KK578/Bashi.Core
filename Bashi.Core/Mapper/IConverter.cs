namespace Bashi.Core.Mapper
{
    public interface IConverter
    {
    }

    public interface IConverter<TIn, TOut> : IConverter
    {
        TOut Map(IMapperContext context, TIn value);
    }
}