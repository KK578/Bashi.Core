namespace Bashi.Core.Mapper
{
    public interface IConverterRegistrationBuilder
    {
        IConverterRegistrationBuilder For<TIn, TOut>();
    }
}