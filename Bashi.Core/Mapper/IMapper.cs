using System.Collections.Generic;

namespace Bashi.Core.Mapper
{
    public interface IMapper
    {
        IReadOnlyList<TOut> Map<TIn, TOut>(IEnumerable<TIn> values);
        TOut Map<TIn, TOut>(TIn value);
    }
}